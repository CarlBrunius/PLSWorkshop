# IRIS Classification

# Load the famous iris data set, inspect it and extract X and Y data
data(iris)
dim(iris)
str(iris)
head(iris)
tail(iris)
Xiris=iris[,1:4] # X data in first 4 columns
Yiris=iris[,5]   # class in the 5th column
## PLS classification with 10 repetitions
# Approx 0.1 min on my computer
IrisPLS=rdCV(X=Xiris,Y=Yiris,nRep=10,nOuter=8,method='PLS',parallel=FALSE)
# Plot overall consensus model
plotMV(IrisPLS)
# Make biplot of consensus model
biplotPLS(IrisPLS$Fit$plsFit,labPlSc=FALSE,colSc=Yiris,colLo = 'blue')
# Tabularize predictions
table(actual=Yiris,rdCV=IrisPLS$yClass)

#####################

# Real Omics examples

#####################

# Regression examples using "freelive" data
rm(list=ls()) # Clear the environment
# Call in relevant packages
library(doParallel)
library(rdCV)
# Classification examples using "mosquito" data
data("mosquito")
#  Xotu2=Microbiota OTU (16S rDNA) from mosquitos captured in 3 different villages in Burkina Faso
#  Yotu=Villages of capture

## PLS classification with 3 repetitions
# Approx 0.4 min on my computer
Class_PLS_3=rdCV(X=Xotu2,Y=Yotu,nRep=3,nOuter=8,method='PLS',parallel=FALSE)
plotMV(Class_PLS_3)
# Parallel processing of the same analysis but with 20 repetitions
# Approx 0.9 min on my computer
nCore=detectCores()-1
cl=makeCluster(nCore)
registerDoParallel(cl)
Class_PLS_20=rdCV(X=Xotu2,Y=Yotu,nRep=20,nOuter=8,method='PLS') 
stopCluster(cl)
# Look at prediction results
plotMV(Class_PLS_20)
# Extract rdCV predictions
yPred_Class=Class_PLS_20$yPred
# Look at PLS biplot
biplotPLS(Class_PLS_20$Fit$plsFit,labPlLo = FALSE,colSc=Y,colLo = 'blue')
# Extract optimum number of components
Class_PLS_20$nComp
# Tabularize predictions
table(actual=Yotu,rdCV=Class_PLS_20$yClass)

data("freelive")
#  XRVIP=LCMS metabolomics of urine samples (selected metabolite features)
#  YR=Dietary consumption of whole grain rye in a free living population
#  IDR=Individual identifier (due to resampling after 2-3 months -> Dependent samples)

## PLS regression with 3 repetitions
# 0.3 min on my computer
Regr_PLS_3=rdCV(X=XRVIP,Y=YR,ID=IDR,nRep=3,nOuter=8,method='PLS',parallel=FALSE) 
plotMV(Regr_PLS_3)
# Parallel processing of the same analysis but with 20 repetitions
# Approx 0.6 min on my computer
nCore=detectCores()-1
cl=makeCluster(nCore)
registerDoParallel(cl)
Regr_PLS_20=rdCV(X=XRVIP,Y=YR,ID=IDR,nRep=20,nOuter=8,method='PLS') 
stopCluster(cl)
plotMV(Regr_PLS_20)
biplotPLS(Regr_PLS_20$Fit$plsFit,labPlLo = FALSE,labPlSc=FALSE,colLo = 'red',xCol = YR)
# Check optimum number of components
Regr_PLS_20$nComp
# Extract rdCV predictions
yPred_Regr=Regr_PLS_20$yPred

data("crisp")
#  crispEM = Effect Matrix of difference between 2 treatments

## ML-PLS with 3 repetitions
# 0.3 min on my computer
ML_PLS_3=rdCV(X=crispEM,ML=TRUE,nRep=3,nOuter=8,method='PLS',parallel=FALSE) 
plotMV(ML_PLS_3)
# Parallel processing of the same analysis but with 20 repetitions
# Approx 0.6 min on my computer
nCore=detectCores()-1
cl=makeCluster(nCore)
registerDoParallel(cl)
ML_PLS_20=rdCV(X=crispEM,ML=TRUE,nRep=20,nOuter=8,method='PLS')
stopCluster(cl)
plotMV(ML_PLS_20)
# Check optimum number of components
ML_PLS_20$nComp
# Biplot function currently doesn't support PLS with only 1 component.
# Extract rdCV predictions
ML_PLS_20$yPred
ML_PLS_20$yClass
ML_PLS_20$miss
# Tabularize output
table(actual=ML_PLS_20$inData$Y,predicted=ML_PLS_20$yClass)
