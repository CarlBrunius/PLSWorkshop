# Call in relevant libraries

library(doParallel)
library(rdCV)

###########################

# Freelive - Regression
# This calculation will take approx 2.5 min on my computer
# It will take much longer time with nPerm=100, nRep=20 and nOuter=8

rm(list=ls()) # Clear the environment
data("freelive") # Load data

nPerm=15 # Low number of permutations just to show the general idea. These permutations take time!!!
permFitness=numeric(nPerm)
nCore=detectCores()-1
cl=makeCluster(nCore)
registerDoParallel(cl)
# Real model
realModel=rdCV(X=XRVIP,Y=YR,ID=IDR,nRep=2*nCore,nOuter=5,method='PLS') 
realFitness=realModel$fitMetric$Q2
# Permutations 
for (p in 1:nPerm) {
  cat('\nPermutation',p,'of',nPerm)
  Yperm=sample(YR)
  permModel=rdCV(X=XRVIP,Y=Yperm,ID=IDR,nRep=2*nCore,nOuter=5,method='PLS') 
  permFitness[p]=permModel$fitMetric$Q2
}
stopCluster(cl)
plotPerm(realFitness,permFitness)


###########################

# Mosquito - Classification
# This calculation will take approx 3.5 min on my computer
# It will take much longer time with nPerm=100, nRep=20 and nOuter=8

rm(list=ls()) # Clear the environment
data("mosquito") # Load data

nPerm=15 # Low number of permutations just to show the general idea. These permutations take time!!!
permFitness=numeric(nPerm)
nCore=detectCores()-1
cl=makeCluster(nCore)
registerDoParallel(cl)
# Real model
realModel=rdCV(X=Xotu2,Y=Yotu,nRep=2*nCore,nOuter=5,method='PLS') 
realFitness=realModel$miss
# Permutations 
for (p in 1:nPerm) {
  cat('\nPermutation',p,'of',nPerm)
  Yperm=sample(Yotu)
  permModel=rdCV(X=Xotu2,Y=Yotu,nRep=2*nCore,nOuter=5,method='PLS') 
  permFitness[p]=permModel$miss
}
stopCluster(cl)
pPerm(realFitness,permFitness) # p=0.6
# plotPerm will not be nice

###########################

# Crisp - Multilevel
# This calculation will take approx 3 min on my computer
# It will take much longer time with nPerm=100, nRep=20 and nOuter=8

rm(list=ls()) # Clear the environment
data("crisp") # Load data

nPerm=15 # Low number of permutations just to show the general idea. These permutations take time!!!
permFitness=numeric(nPerm)
nCore=detectCores()-1
cl=makeCluster(nCore)
registerDoParallel(cl)
# Real model
realModel=rdCV(X=crispEM,ML=T,nRep=2*nCore,nOuter=5,method='PLS') 
realFitness=realModel$miss
# Permutations 
for (p in 1:nPerm) {
  cat('\nPermutation',p,'of',nPerm)
  Yperm=sample(c(-1,1),size = nrow(crispEM),replace=T)
  permModel=rdCV(X=crispEM,Y=Yperm,ML=T,nRep=2*nCore,nOuter=5,method='PLS') 
  permFitness[p]=permModel$miss
}
stopCluster(cl)
plotPerm(realFitness,permFitness)
# p=0.058 -> Need more permutations (and better parameter settings) to see if this is significant.
