# PLS Workshop at the Swedish NMR Center 11-12 October 2017
## By Carl Brunius, Chalmers
### Feedback
Please provide feedback so I can continue to develop workshops and tutorials on data analysis :)  
https://goo.gl/forms/aawfb1v0GOmN08eW2

_11 October_  
Lecture on PLS flavours (regression, DA, multiclass, multilevel, effect projection), cross-validation and permutation tests.  
Slides are found in the document "20171011 Multivariate workshop.pdf".

_12 October_  
Practical BYOD (Bring Your Own Data) session  
Work on your own data or use data available from the `Data` folder  
You can use whichever program you wish for data analysis.  
If you feel brave & wish to try out R for your analysis, then there are some example scripts in the `Scripts` folder and a tutorial: "A Brief Tutorial on Multivariate Analysis in R.docx".   
To use the `rdCV` function, you'll need to install the `rdCV` package and installation instructions can be found in the tutorial

